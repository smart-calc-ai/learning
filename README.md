# Smart Calc AI - learning

## create project
```shell
python3 -m venv ./.venv
```

```shell
source .venv/bin/activate
```

```shell
pip freeze > requirements.txt
```

```shell
pip install -r ./requirements.txt
```


## iniciar a aplicação
```shell
gunicorn -w 4 -b 0.0.0.0:5000 --log-level=debug app:app
```

