# Use uma imagem base leve do Python
FROM python:3.12.3-bullseye

# Define o diretório de trabalho dentro do contêiner
WORKDIR /usr/src/app

# Copie os arquivos de requisitos e instale as dependências
COPY requirements.txt ./
# Copie o código fonte da aplicação
COPY src/ .

RUN ls -l /usr/src/app
RUN pip install -r requirements.txt

# Defina a porta através da variável de ambiente
ENV PORT=8080
ENV CONN_DB="mongodb://mongo:27017/"

# Execute o aplicativo quando o contêiner for iniciado usando Gunicorn
CMD ["gunicorn", "-w", "4", "-b", "0.0.0.0:8080", "--log-level=debug", "app:app"]
