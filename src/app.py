from http import HTTPStatus
import logging
from operator import imod
import os
from flask import Flask, jsonify
from controller.calculator_parameters import calculator_param_controller
from controller.data_generator import generator_controller
from controller.quote import quote_controller
from controller.training import training_controller

VERSION_API = "v1/api"

app = Flask(__name__)

app.register_blueprint(quote_controller, url_prefix=f"/learning/{VERSION_API}/quote")
app.register_blueprint(
    calculator_param_controller, url_prefix=f"/learning/{VERSION_API}/calculator"
)
app.register_blueprint(
    generator_controller, url_prefix=f"/learning/{VERSION_API}/generator"
)
app.register_blueprint(
    training_controller, url_prefix=f"/learning/{VERSION_API}/training"
)


if __name__ != "__main__":
    gunicorn_logger = logging.getLogger("gunicorn.error")
    app.logger.handlers = gunicorn_logger.handlers
    app.logger.setLevel(gunicorn_logger.level)


@app.errorhandler(ValueError)
def handle_value_error(e):
    return jsonify({"error": str(e)}), HTTPStatus.BAD_REQUEST


@app.errorhandler(Exception)
def handle_exception_error(e):
    return jsonify({"error": str(e)}), HTTPStatus.BAD_REQUEST


# Execute o aplicativo Flask
if __name__ == "__main__":
    port = int(os.environ.get("PORT", 5000))
    app.run(port=port, debug=True)
