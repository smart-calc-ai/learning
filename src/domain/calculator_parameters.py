from datetime import datetime
from repository.calculator_parameters import CalculatorParametersRepository


repository = CalculatorParametersRepository()


class CalculatorParametersSevice:

    _instance = None

    def __new__(cls):
        if cls._instance is None:
            cls._instance = super().__new__(cls)

        return cls._instance

    def save(self, data: dict[str, any]) -> str:
        last = repository.find_last()
        if last is not None:
            update_data = {"state": "last", "updated_at": datetime.now().isoformat()}
            repository.update(last["_id"], update_data)

        data["state"] = "current"
        data["created_at"] = datetime.now().isoformat()
        repository.save(data)
