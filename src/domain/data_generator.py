from turtle import st
import joblib
import numpy as np
from sklearn.decomposition import PCA
from sklearn.model_selection import train_test_split
import time
import pandas as pd
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPRegressor
from sklearn.metrics import mean_squared_error

from repository.data_generator import DataGeneratorRepository

COUNT_ROWS = 10000

FILE_NAME = "training_test_sets.joblib"

repository = DataGeneratorRepository()


class DataGeneratorSevice:

    _instance = None

    def __new__(cls):
        if cls._instance is None:
            cls._instance = super().__new__(cls)
            cls._instance._ml_pregressor = None
            cls._instance._scaler = None

        return cls._instance

    def calculate(self, body: dict[str, any]) -> dict[str, any]:
        if self._scaler is not None and self._ml_pregressor is not None:
            data = [value for key, value in body.items()]
            new_data = self._scaler.transform([data])

            forecast = self._ml_pregressor.predict(new_data)
            return {"forecast": forecast[0]}
        
        raise Exception("It is necessary to load the AI ​​pre-training and AI training")

    def training(self, uuid: str) -> dict[str, any]:
        X_train, X_test, y_train, y_test = repository.find_train(uuid, FILE_NAME)

        self._scaler = StandardScaler()
        X_train_scaled = self._scaler.fit_transform(X_train)
        X_test_scaled = self._scaler.transform(X_test)

        self._ml_pregressor = MLPRegressor(
            hidden_layer_sizes=(300, 250),
            activation="relu",
            solver="adam",
            max_iter=10000,
            learning_rate_init=0.0001,
            random_state=0,
        )
        self._ml_pregressor.fit(X_train_scaled, y_train)

        y_pred_train = self._ml_pregressor.predict(X_train_scaled)
        y_pred_test = self._ml_pregressor.predict(X_test_scaled)

        mse_train = mean_squared_error(y_train, y_pred_train)
        mse_test = mean_squared_error(y_test, y_pred_test)

        joblib.dump(self._ml_pregressor, 'mlp_regressor_model.pkl')


        return {"mse_train": mse_train, "mse_test": mse_test}

    def pre_training(self) -> None:
        hour_value = 55.0
        employee_hour_value = 12.5
        margin_per_km_driven = 0.005
        margin_vehicle_maintenance = 0.03
        federal_tax_percentage = 0.04
        margin_material_profit = 0.2
        margin_tools = 0.01

        estimated_hours = np.random.randint(1, 24, COUNT_ROWS)
        complexities = np.random.randint(0, 3, COUNT_ROWS)
        personal_cost = np.random.uniform(1.00, 250.00, COUNT_ROWS)
        embedded_material_cost = np.random.uniform(250.00, 5000.00, COUNT_ROWS)
        material_cost_buy = np.random.uniform(250.00, 5000.00, COUNT_ROWS)
        helper_time_hours = estimated_hours
        value_professional_hours = np.full(COUNT_ROWS, hour_value)
        employee_hourly_rate = np.full(COUNT_ROWS, employee_hour_value)
        margin_material_profit_full = np.full(COUNT_ROWS, margin_material_profit)
        distance_customer_km = np.random.randint(0, 100, COUNT_ROWS)
        total_employee_hourly_rate = helper_time_hours * employee_hourly_rate
        total_professional_hourly_rate = value_professional_hours * estimated_hours
        total_distance_customer_km = (
            hour_value * margin_per_km_driven * distance_customer_km
        )
        vehicle_maintenance_value = (
            total_professional_hourly_rate * margin_vehicle_maintenance
        )
        total_tax_amount = total_professional_hourly_rate * federal_tax_percentage
        total_margin_material_profit = (
            total_professional_hourly_rate * margin_material_profit
        )
        total_margin_tools = total_professional_hourly_rate * margin_tools
        gross_price_service = (
            personal_cost
            + material_cost_buy
            + (embedded_material_cost * margin_material_profit)
            + total_employee_hourly_rate
            + total_professional_hourly_rate
            + total_distance_customer_km
            + vehicle_maintenance_value
            + total_tax_amount
            + total_margin_material_profit
            + total_margin_tools
        )

        price_per_complexity = []
        for i in range(COUNT_ROWS):
            if complexities[i] == 1:
                price_per_complexity.append(gross_price_service[i] * 1.1)
            elif complexities[i] == 2:
                price_per_complexity.append(gross_price_service[i] * 1.2)
            else:
                price_per_complexity.append(gross_price_service[i])

        X = np.array(
            [
                estimated_hours,
                complexities,
                personal_cost,
                embedded_material_cost,
                material_cost_buy,
                total_employee_hourly_rate,
                total_professional_hourly_rate,
                total_distance_customer_km,
                margin_material_profit_full,
                total_margin_material_profit,
            ]
        ).T

        y = np.array(price_per_complexity)

        X_train, X_test, y_train, y_test = train_test_split(
            X, y, test_size=0.3, random_state=0
        )

        id = repository.save(FILE_NAME, X_train, X_test, y_train, y_test)
        return {"train_id": id}
