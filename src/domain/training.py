import asyncio
from http import HTTPStatus
import os
from time import sleep
from flask import request
import pandas as pd
from datetime import date
from werkzeug.datastructures.file_storage import FileStorage
from werkzeug.datastructures.structures import ImmutableMultiDict
import logging


ALLOWED_EXTENSIONS = {"csv"}
UPLOAD_FOLDER = "uploads"

if not os.path.exists(UPLOAD_FOLDER):
    os.makedirs(UPLOAD_FOLDER)


class TrainingSevice:

    _instance = None

    def __new__(cls):
        if cls._instance is None:
            cls._instance = super().__new__(cls)

        return cls._instance

    def start_training(self, filename: str):
        logging.warning(f"Training started for file: {filename}")
        logging.warning(f"Training completed for file: {filename}")

    def upload(self, files: ImmutableMultiDict) -> dict[str, any]:

        if "file" not in request.files:
            return {"error": "Arquivo não permitido"}

        file: FileStorage = files["file"]

        # Verifica se o arquivo tem um nome e é do tipo permitido
        if file.filename == "" or not _allowed_file(file.filename):
            return {"error": "Arquivo inválido"}

        folder = f"{UPLOAD_FOLDER}/{date.today().isoformat()}"

        if not os.path.exists(folder):
            os.makedirs(folder)

        file.save(os.path.join(folder, file.filename))
        return {
            "success": "Arquivo enviado com sucesso",
            "filename": f"{folder}/{file.filename}",
        }


def _allowed_file(filename):
    return "." in filename and filename.rsplit(".", 1)[1].lower() in ALLOWED_EXTENSIONS
