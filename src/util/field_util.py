class FieldUtil:
    @staticmethod
    def field_required(key, expected_type, **kwargs):
        if key not in kwargs:
            raise ValueError(f"The {key} field is mandatory.")
        if not isinstance(kwargs[key], expected_type):
            raise ValueError(f"The '{key}' field must be of type {expected_type.__name__}.")
