from flask import Blueprint, request, jsonify
from http import HTTPStatus

from domain.calculator_parameters import CalculatorParametersSevice
from repository.calculator_parameters import CalculatorParametersRepository
from util.field_util import FieldUtil


repository = CalculatorParametersRepository()
service = CalculatorParametersSevice()

calculator_param_controller = Blueprint("calculator", __name__)


@calculator_param_controller.route("/parameters", methods=["POST"])
def save():
    body = request.get_json(force=True)
    parameters = CalculatorParametersRequest.loan(**body)
    service.save(parameters)
    return "", HTTPStatus.CREATED


@calculator_param_controller.route("/parameters", methods=["GET"])
def find_all():
    parameters = repository.find_all(
        request.args.get("page"), request.args.get("limit")
    )
    return jsonify(parameters), HTTPStatus.OK

@calculator_param_controller.route("/parameters/current", methods=["GET"])
def find_curret():
    parameters = repository.find_last()
    return jsonify(parameters), HTTPStatus.OK

class CalculatorParametersRequest:
    hour_value: float
    employee_hour_value: float
    federal_tax_percentage: float
    margin_vehicle_maintenance: float
    margin_tools: float
    margin_per_km_driven: float
    margin_material_profit: float

    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)

    @classmethod
    def loan(cls, **kwargs):
        FieldUtil.field_required("hour_value", float, **kwargs)
        FieldUtil.field_required("employee_hour_value", float, **kwargs)
        FieldUtil.field_required("federal_tax_percentage", float, **kwargs)
        FieldUtil.field_required("margin_vehicle_maintenance", float, **kwargs)
        FieldUtil.field_required("margin_tools", float, **kwargs)
        FieldUtil.field_required("margin_per_km_driven", float, **kwargs)
        FieldUtil.field_required("margin_material_profit", float, **kwargs)
        return vars(cls(**kwargs))
