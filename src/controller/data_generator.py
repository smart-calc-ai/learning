from http import HTTPStatus
import json
from flask import Blueprint, jsonify, request

from domain.data_generator import DataGeneratorSevice


generator_controller = Blueprint("generator", __name__)

service = DataGeneratorSevice()


@generator_controller.route("/pre-training", methods=["GET"])
def pre_training():
    
    return jsonify(service.pre_training()), HTTPStatus.OK


@generator_controller.route("/training", methods=["GET"])
def find():
    uuid = request.args.get("uuid")
    return jsonify(service.training(uuid)), HTTPStatus.OK
