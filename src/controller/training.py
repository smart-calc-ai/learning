import os
from flask import Blueprint, request, jsonify
from http import HTTPStatus

from domain.training import TrainingSevice

ALLOWED_EXTENSIONS = {"csv"}
UPLOAD_FOLDER = "uploads"

if not os.path.exists(UPLOAD_FOLDER):
    os.makedirs(UPLOAD_FOLDER)

training_controller = Blueprint("training", __name__)
service = TrainingSevice()


@training_controller.route("/start", methods=["POST"])
def start_training():
    body: dict[str, any] = request.get_json(force=True)
    service.start_training(body.get("filename"))
    return jsonify({"success": "Processo de treiamento iniciado"}), HTTPStatus.OK


@training_controller.route("/upload", methods=["POST"])
def upload():
    resp = service.upload(request.files)
    if "error" in resp:
        return jsonify(resp), HTTPStatus.BAD_REQUEST

    return jsonify(resp), HTTPStatus.OK
