from http import HTTPStatus
from flask import Blueprint, jsonify, request

from domain.data_generator import DataGeneratorSevice

quote_controller = Blueprint("quote", __name__)

service = DataGeneratorSevice()

@quote_controller.route("/calculate", methods=["POST"])
def calculate():
    body = request.get_json(force=True)
    return jsonify(service.calculate(body)), HTTPStatus.OK


@quote_controller.route("/", methods=["POST"])
def save():
    body = request.get_json(force=True)
    return "", HTTPStatus.OK