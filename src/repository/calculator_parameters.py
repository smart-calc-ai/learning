import os
from bson import ObjectId
import pymongo


class CalculatorParametersRepository:

    _instance = None

    def __new__(cls):
        if cls._instance is None:
            connect_db = os.environ.get(
                "CONN_DB", "mongodb://admin:admin123@localhost:27017/"
            )
            client = pymongo.MongoClient(connect_db)
            db = client["smart_calc_learning"]
            collection = db["calculator_parameters"]
            cls._instance = super().__new__(cls)
            cls._instance._client = client
            cls._instance._collection = collection

        return cls._instance

    def save(self, data: dict[str, any]) -> int:
        return str(self._collection.insert_one(data).inserted_id)

    def update(self, id: str, update_data: dict[str, any]) -> str:
        filter_query = {"_id": ObjectId(id)}
        result = self._collection.update_one(filter_query, {"$set": update_data})
        return result.modified_count

    def find_all(self, page=None, limit=None):
        if page is not None and limit is not None:
            skip_count = (int(page) - 1) * int(limit)
            documentos = self._collection.find().sort("_id", pymongo.DESCENDING).skip(skip_count).limit(int(limit))
        else:
            documentos = self._collection.find().sort("_id", pymongo.DESCENDING)

        documentos_json = []
        for documento in documentos:
            documento["_id"] = str(documento["_id"])
            documentos_json.append(documento)
        return documentos_json

    def find_last(self):
        last_document = self._collection.find_one(sort=[("_id", pymongo.DESCENDING)])
        if last_document:
            last_document["_id"] = str(last_document["_id"])
        return last_document
