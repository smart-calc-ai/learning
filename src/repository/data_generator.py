import os
from bson import ObjectId
import joblib
import pymongo
from gridfs import GridFS


class DataGeneratorRepository:

    _instance = None

    def __new__(cls):
        if cls._instance is None:
            connect_db = os.environ.get(
                "CONN_DB", "mongodb://admin:admin123@localhost:27017/"
            )
            client = pymongo.MongoClient(connect_db)
            db = client["smart_calc_learning"]
            collection = db["data_genarator"]
            cls._instance = super().__new__(cls)
            cls._instance._db = db
            cls._instance._client = client
            cls._instance._collection = collection

        return cls._instance

    def save(self, filename: str, X_train, X_test, y_train, y_test) -> int:
        joblib.dump((X_train, X_test, y_train, y_test), filename)
        fs = GridFS(self._db)
        with open(filename, "rb") as f:
            file_id = fs.put(f)
            
        os.remove(filename)
        return str(file_id)

    def find_train(self, file_id: str, filename: str):
        fs = GridFS(self._db)
        file = fs.find_one({"_id": ObjectId(file_id)})
        if file is None:
            raise FileNotFoundError(f"Arquivo com ID {file_id} não encontrado no GridFS")
        with open(filename, "wb") as f:
            f.write(file.read())
        return joblib.load(filename)
